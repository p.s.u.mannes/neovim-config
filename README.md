# README

## Setup on .deb OS
```
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/Meslo.zip
unzip Meslo.zip -d Meslo
sudo cp Meslo/'Meslo LG S Regular Nerd Font Complete.ttf' /usr/share/fonts/truetype/
sudo fc-cache -f -v
sudo apt-get install ripgrep
sudo apt install nodejs npm
sudo apt-get install python3-venv
```
### Inside nvim

```
:PackerSync
:MasonInstall <NameOfLinter/Formatter>
```

## Shortcut examples

```
C- o: next buffer
C- i: prev buffer

:vs: split window
:sv: split window vertical

gf: get function definition, implementation, references

] d: jump to code error

<leader>ca: code action to fix error

<leader>rn: rename+refactor function/variable

:wa: save changes across all files

```

## Windows additional setup
### Clipboard
https://github.com/neovim/neovim/wiki/FAQ

In WSL:
curl -sLo/tmp/win32yank.zip https://github.com/equalsraf/win32yank/releases/download/v0.0.4/win32yank-x64.zip
unzip -p /tmp/win32yank.zip win32yank.exe > /tmp/win32yank.exe
chmod +x /tmp/win32yank.exe
sudo mv /tmp/win32yank.exe /usr/local/bin/

vim.opt.clipboard="unnamedplus"

### WSL font
install powerline fonts:
https://github.com/powerline/fonts/blob/master/DejaVuSansMono/DejaVu%20Sans%20Mono%20for%20Powerline.ttf


