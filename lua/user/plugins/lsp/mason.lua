local mason_status, mason = pcall(require, "mason")
if not mason_status then
    return
end

local mason_lspconfig_status, mason_lspconfig = pcall(require, "mason-lspconfig")
if not mason_lspconfig_status then
    return
end

-- import mason-null-ls plugin safely
local mason_null_ls_status, mason_null_ls = pcall(require, "mason-null-ls")
if not mason_null_ls_status then
  return
end

mason.setup()

-- https://github.com/williamboman/mason-lspconfig.nvim
mason_lspconfig.setup({
    ensure_installed = {
        "ansiblels",
        "bashls",
        "jdtls",
        "jsonls",
        "marksman",
        "pylsp",
        "yamlls",
        "lemminx",
        "sqlls",
    }
})

mason_null_ls.setup({
  -- list of formatters & linters for mason to install
  ensure_installed = {
    "black", -- ts/js formatter
    "flake8",
    "yamllint",
    "ansible-lint"
  },
  -- auto-install configured formatters & linters (with null-ls)
  automatic_installation = true,
})
