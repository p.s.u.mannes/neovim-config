local cmp_status, cmp = pcall(require, "cmp")
if not cmp_status then
    return
end

local luasnip_status, ls = pcall(require, "luasnip")
if not luasnip_status then
    return
end

local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

-- load friendly-snippets
require("luasnip/loaders/from_vscode").lazy_load()

-- Snippet specific keymaps
function jump_to_next_insert()
    if ls.expand_or_jumpable() then
        if ls.expand_or_jump() then
            return true
        end
    else
        return false
    end
end
vim.api.nvim_set_keymap("i", "<Tab>", 'v:lua.jump_to_next_insert() ? "" : "<Tab>"', { noremap = true, expr = true, silent = true })


local yaml_snippets = {
    s({trig="- name",
        name="ansible task",
        dscr="create a new task"}, fmt([[
        - name: {name}
          ansible.builtin.{module}:
            {opt}: {value}
    ]], {
        name = i(1, "name"),
        module = i(2, "module"),
        opt = i(3, "opt"),
        value = i(0, "value"),
    })),

    s({trig="- name",
        name="ansible playbook",
        dscr="create a new ansible playbook"}, fmt([[
        - name: {name}
          hosts: {hosts}
          tasks:
            - nam{finish} 
    ]], {
        name = i(1, "name"),
        hosts = i(2, "hosts"),
        finish = i(0, "e"),
    })),
}


cmp.setup({
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ["<C-k>"] = cmp.mapping.select_prev_item(), -- previous suggestion
    ["<C-j>"] = cmp.mapping.select_next_item(), -- next suggestion
    ["<C-b>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-Space>"] = cmp.mapping.complete(), -- show completion suggestions
    ["<C-e>"] = cmp.mapping.abort(), -- close completion window
    ["<CR>"] = cmp.mapping.confirm({ select = false }),
  }),
  -- sources for autocompletion
  sources = cmp.config.sources({
    { name = "nvim_lsp" }, -- lsp
    { name = "luasnip" }, -- snippets
    { name = "buffer" }, -- text in current buffer
    { name = "path" }, -- file system paths
  }),

  -- luasnip.add_snippets("all", {
  --   luasnip.snippet("trigger", { luasnip.text_node("Wow! Text!") }),
  -- })

  ls.add_snippets("yaml", yaml_snippets),
  ls.add_snippets("yml", yaml_snippets),

})
