local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

-- import packer safely
local status, packer = pcall(require, "packer")
if not status then
    return
end

return require('packer').startup(function(use)
    -- package manager
    use('wbthomason/packer.nvim')

    -- lua functions that many plugins use
    use("nvim-lua/plenary.nvim")

    -- my plugins here
    use { "bluz71/vim-moonfly-colors", as = "moonfly" }

    -- tmux & split window navigation
    use("christoomey/vim-tmux-navigator")

    -- maximize windows and restore to original size
    use("szw/vim-maximizer")

    -- essential plugins
    --
    -- look at docs, good for changing string types:
    -- ysw',ds',csw'"
    use("tpope/vim-surround")
    -- gr to copy text and replace
    -- e.g. yw word1 -> grw word2 -> word1 word1
    use("vim-scripts/ReplaceWithRegister")

    -- (un)commenting with gcc, gc{n}j, gc{n}k
    use("numToStr/Comment.nvim")

    -- file explorer
    use {
    'kyazdani42/nvim-tree.lua',
    requires = {
        'nvim-tree/nvim-web-devicons', -- optional, for file icons
    },
    }

    -- statusline
    use("nvim-lualine/lualine.nvim")

    -- fuzzy finding
    -- telescope performance enhancer, optional
    use({ "nvim-telescope/telescope-fzf-native.nvim", run = "make" })
    -- telescope
    use({ "nvim-telescope/telescope.nvim", branch = "0.1.x" })

    -- autocompletion
    use("hrsh7th/nvim-cmp")
    use("hrsh7th/cmp-buffer")
    use("hrsh7th/cmp-path")

    use("L3MON4D3/LuaSnip")
    use("saadparwaiz1/cmp_luasnip")
    use("rafamadriz/friendly-snippets")

    -- managing and installing lsp servers, linters, & formatters
    use("williamboman/mason.nvim")
    use("williamboman/mason-lspconfig.nvim")

    -- configuring lsp servers
    use("neovim/nvim-lspconfig")
    -- allow lsp servers to appear in autocompletion
    use("hrsh7th/cmp-nvim-lsp")
    -- enhanced UI for lsp
    use({ "glepnir/lspsaga.nvim", branch = "main" })
    -- add icons to autocompletion
    use("onsails/lspkind.nvim")

    -- formatting & linting
    use("jose-elias-alvarez/null-ls.nvim") -- configure formatters & linters
    use("jayp0521/mason-null-ls.nvim") -- bridges gap between mason & null-ls
    
    -- git integration
    use("lewis6991/gitsigns.nvim") -- show line modifications on left hand side

    -- auto session
    -- auto-save current session, resume with command "nvim"
    use("rmagatti/auto-session")

    -- different linter
    use("mfussenegger/nvim-lint")

    if packer_bootstrap then
        require('packer').sync()
    end
end)
